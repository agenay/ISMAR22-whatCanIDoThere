# ISMAR 2022 - What can I do there?

![enter image description here](https://i.postimg.cc/8PxQyvJv/teaser-v4.png)

Welcome to the prototype project that comes with the paper **"What Can I Do There? Controlling AR Self-Avatars to Better Perceive Affordances of the Real World"**, by Adélaïde Genay, Anatole Lécuyer, and Martin Hachet, accepted at ISMAR 2022 Conference tracks.

This project contains the base for the implementation of a system allowing users to manipulate an avatar in the third person in Augmented Reality (AR). It runs on Unity 2019.4.16f1 and is meant to be deployed on Microsoft Hololens 2 display.

- Please note that <ins>we do not provide the entire implementation</ins> as we used proprietary software packages that need to be paid for (PuppetMaster). Therefore, the project will not compile. We're still making the rest of our code available for people to use.
- We did not push third-party libraries like MRTK, and more particularly, the Azure Kinect SDKs that are very heavy and dependent on your installation. 
- Lastly, part of the code we make available is inspired from or borrowed from the open source content. We reference where this code was taken from in the files where it was used.


## Requirements

**Hardware**
- Computer with Windows 10 April 2018 (Version 1803, OS Build 17134) release (x64) or a later version
- Seventh Gen Intel® CoreTM i3 Processor (Dual Core 2.4 GHz with HD620 GPU or faster)
- Microsoft Hololens 2
- Microsoft Azure Kinect sensor: https://www.microsoft.com/en-us/d/azure-kinect-dk/8pp5vxmd9nhq?activetab=pivot:overviewtab
- 4 GB Memory
- Dedicated USB3 port
- Graphics driver support for OpenGL 4.4 or DirectX 11.0
- Optional: Bluetooth gamepad (XBOX X/S series)

**Software**
- Unity 2019.4.16f1
- Visual Studio 2019
- Windows Mixed Reality Runtime v109

**Unity packages**
- MRTK Foundation, Extensions, Tools, and Standard Assets packages v2.6.1: https://docs.microsoft.com/en-us/learn/modules/mixed-reality-toolkit-project-unity/
- OpenXR plugin v1.3.1
- TextMeshPro package v2.1.4
- PuppetMaster package v1.1.0 for the Puppeteering mode, available here: https://assetstore.unity.com/packages/tools/physics/puppetmaster-48977
- Azure Kinect Sensor SDK v1.4.1: https://docs.microsoft.com/en-us/azure/kinect-dk/  
- Azure Kinect Body Tracking SDK v1.0.1 (same link)

## Usage

This implementation is a functional prototype for Hololens 2, but it was not hard-tested so it may contain some bugs.

We provide one female avatar model generated with [Virtual Caliper](https://virtualcaliper.is.tue.mpg.de/). We implemented three control modes to manipulate the avatar:

- **Physical Control mode**: the avatar is controlled through an XBOX gamepad, as in video games (left joystick for moving, A for jumping, etc.). Also works with keyboard arrow keys.

- **Puppeteering mode**: spheres will appear around the avatar. Selecting them with mid-air hand interactions recognized by the headset (pinching, ray-casting) allows one to manipulate limbs individually.

- **Body tracking mode**: the avatar is animated with body tracking provided by a Microsoft Azure Kinect.

These modes are selectable from a hand menu that appears when you look at your hand.

**Build and deploy**

In Unity, the project build mode has to be switched to Universal Windows Platform (see Player Settings in Unity) with "HoloLens" as the target device, and an x64 architecture.

Once the build is finished, you can open the resulting .sln file in Visual Studio, plug your Hololens, and deploy the project using the following instructions:
https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/build-and-deploy-to-hololens



  

### Create your scene with your own avatar

You may create your personal avatar with Virtual Caliper as we did. You can install and follow the creation procedure available here: https://virtualcaliper.is.tue.mpg.de/. This project also works with [UMA 2](https://assetstore.unity.com/packages/3d/characters/uma-2-unity-multipurpose-avatar-35611) avatars. 
Once this is done, follow the steps below.

**In Unity,**

- Add the MixedReality Toolkit and Playspace to your scene by opening the "Mixed Reality Toolkit" tab > "Add to the scene and configure...".
- Click on the MixedReality Toolkit gameobject. Select the following Configuration Profile by clicking on the drop-down menu of its component: "ISMARConfigurationProfile"
- Add your avatar to the Assets of the Unity project. It has to be imported as a humanoid and needs to be rigged with this bone hierarchy: https://docs.microsoft.com/fr-fr/azure/kinect-dk/body-joints
- Add your avatar to the scene you're working on. Attach the material you want (e.g. Assets/Material/glowingCyanMaterial)
- In the Animator component, add the following Controller: "AvatarController".
- Add the component called "Object Manipulator" from the MRTK package to your avatar's root gameobject, as well as the "Rotation Axis Constraint" script. A "Constraint Manager" should be automatically added.
- In the "Rotation Axis Constraint" component, set the parameter "Contraint On Rotation" to X and Z.
- ONLY if you don't want to use the Physical Control Mode: add a Capsule Collider to your avatar's root and edit its boundaries to fit the avatar's size.
- In Assets/Prefabs, find the "UI" prefab and drag it into the scene. It contains the virtual hand menu allowing to switch modes with AR buttons.  

**For the Physical Control mode**

- Add the component called "Physical Control Mode" to your avatar's gameobject (located in Assets/Scripts). This will automatically add a "Character Controller" component to your avatar. Note that the avatar should have a Skinned Mesh Renderer for the avatar's collider to be resized automatically.
- Change the parameters of the "Physical Control Mode" as you want them to be.
- Currently, the Physical Control mode was implemented to let the avatar stand idle, walk/run (left joystick), jump (A), and crouch (left trigger). You can modify the Physical Control Mode script to add new commands; remember to add them in Player Settings > Input Manager.

**For the Body Tracking Mode**

- In Assets/Prefabs, find the "Kinect4AzureTracker" prefab and slide it into your scene.
- Create an empty gameobject named "BodyTrackingManager" and add the component called "Config Loader". Also, add the component called "Body Tracking" and slide the "Kinect4AzureTracker" gameobject to the "Tracker" parameter of this component.
- Add the component called "Body Tracking Mode" to your avatar's gameobject (located in Assets/Scripts). Assign the "Kinect4AzureTracker" gameobject in the "Kinect Device" parameter of this component. Unfold the "Kinect4AzureTracker" gameobject in the hierarchy to find its child called "pelvis". Slide this gameobject to the "Tracker Root Position" parameter of the "Body Tracking Mode" component on your avatar.

**For the Puppeteering mode**

- Add a "Biped Ragdoll Creator" component to the avatar's gameobject. The bones should be automatically configured in the component if your model is properly rigged. Press "Create a Ragdoll", and then press "Done"
- Add a "Puppet Master" component to your avatar's gameobject. Click the "Set Up PuppetMaster" button. This will attach your avatar's gameobject to a new parent gameobject called "[avatar-name] Root". We'll call this gameobject the "Avatar Root", and the previous one the "Avatar Child". There are also two other new gameobjects: PuppetMaster and Behaviours (children of Avatar Root, alongside Avatar Child).
- Click on the newly created PuppetMaster gameobject. In the "Puppet Master" component of this gameobject, set the Mode to "Kinematic". Check the following boxes: "Angular Pinning", "Angular Limits", and "Internal Collisions". The rest should remain as default.
- Add the component called "Puppeteering Mode" (located in Assets/Scripts) to the Avatar Child gameobject. This will automatically add an "Animator IK" script too.
- In Assets/Prefabs, find the "Targets" prefab and slide it into the "Targets Prefab" parameter of this component. In the bone hierarchy of your avatar, find the right hand, left hand, right foot, and left foot transforms of your avatar model and attach them to the associated parameters of the "Puppeteering Mode" component.
- Slide the "UI" gameobject to the UI parameter of the "Puppeteering Mode" component.

**Avatar Manager**

- Attach the component called "Avatar Manager" (located in Assets/Scripts) to the Avatar Root gameobject. Set the UI gameobject in the "UI" parameter of the "Avatar Manager" component.

- If you want to spawn the avatar in a particular location, create an empty gameobject and place it in your scene. Then, slide it into the parameters of the "Avatar Manager" component.

  

## License

This project is under the MIT license. We are planning to continue improving this project, but you may already use and modify it freely for your own projects. Please kindly reference us if you do.