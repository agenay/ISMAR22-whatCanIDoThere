﻿using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using UnityEngine;

/// <summary>
/// Provides functions to manage the mode menu attached to the user's hand.
/// </summary>
public class UIManager : MonoBehaviour
{
    public GameObject puppeteeringModeButton;
    public GameObject physicalcontrolModeButton;
    public GameObject bodyTrackingModeButton;

    //Puppeteering mode menu (only contains the Rotation button for now)
    public GameObject puppeteeringModeMenu;
    public GameObject rotationButton;
    //[SerializeField] private GameObject resetButton;

    [HideInInspector] public AvatarManager avatarManager;

    void Start()
    {
        puppeteeringModeMenu.gameObject.SetActive(false);

        // When a mode is clicked, launch the corresponding mode "start" function (in AvatarManager)
        if (avatarManager)
        {
            puppeteeringModeButton.GetComponentInChildren<ButtonConfigHelper>().OnClick.AddListener(
                avatarManager.PuppeteeringMode_Start);
            physicalcontrolModeButton.GetComponentInChildren<ButtonConfigHelper>().OnClick.AddListener(
                avatarManager.PhysicalcontrolMode_Start);
            bodyTrackingModeButton.GetComponentInChildren<ButtonConfigHelper>().OnClick.AddListener(
                avatarManager.BodytrackingMode_Start);

            // Rotation button
            rotationButton.GetComponentInChildren<ButtonConfigHelper>().OnClick.AddListener(
                avatarManager.PuppeteeringMode_ToggleRotation);
        }
        else
        {
            Debug.LogError("[UI] Please add an AvatarManager to you avatar's root gameobject for the UI to work.");
        }
    }

    // Remove the underline style from the selected mode
    public void ResetHandMenuAppearance()
    {
        SetButtonTextStyle(puppeteeringModeButton, TMPro.FontStyles.Normal);
        SetButtonTextStyle(physicalcontrolModeButton, TMPro.FontStyles.Normal);
        SetButtonTextStyle(bodyTrackingModeButton, TMPro.FontStyles.Normal);
        SetButtonTextStyle(rotationButton, TMPro.FontStyles.Normal);
    }

    // Set the button text style to the given TextMeshPro style.
    public void SetButtonTextStyle(GameObject button, TMPro.FontStyles style)
    {
        button.GetComponentInChildren<TextMeshPro>().fontStyle = style;
    }

}
