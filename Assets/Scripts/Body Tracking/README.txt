# Scripts/Body Tracking folder

These scripts were taken from the following Github repository: 
    Azure-Kinect-Samples https://github.com/microsoft/Azure-Kinect-Samples/tree/master/body-tracking-samples/sample_unity_bodytracking

They all belong to Copyright (c) Microsoft Corporation and are under the MIT License.