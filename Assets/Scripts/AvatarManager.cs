﻿using UnityEngine;

/// <summary>
/// MAIN SCRIPT
/// Interface with the avatar's control and positioning. Allows switching control modes and
/// keeps track of the current mode in use. Adapts the UI upon switching control modes.
/// </summary>
public class AvatarManager : MonoBehaviour
{
    // Mode menu
    public UIManager UI;

    // Where to spawn the avatar
    [SerializeField] private Transform spawnLocation;

    private PuppeteeringMode puppeteeringMode;
    private PhysicalControlMode physicalControlMode;
    private BodyTrackingMode bodyTrackingMode;

    private void Awake()
    {
        // Fetch components
        puppeteeringMode = GetComponentInChildren<PuppeteeringMode>();
        physicalControlMode = GetComponentInChildren<PhysicalControlMode>();
        bodyTrackingMode = GetComponentInChildren<BodyTrackingMode>();
        UI.avatarManager = this;
    }

    // Spawn the avatar
    void Start()
    {
        // Put avatar at given spawn position
        if (spawnLocation)
            transform.position = spawnLocation.position;
        // Otherwise, the spawn position is 1.5 meters ahead of the user.
        else
        {
            transform.position = Camera.main.transform.position + new Vector3(0, -1f, 1.5f);
            transform.rotation = new Quaternion(0, -180, 0, 0);
        }

        StopAllModes();

        // Reset mode buttons style
        UI.ResetHandMenuAppearance();
    }

    private void StopAllModes()
    {
        UI.ResetHandMenuAppearance();

        puppeteeringMode.StopPuppeteeringMode();
        bodyTrackingMode.enabled = false;
        physicalControlMode.enabled = false;
    }

    public void PuppeteeringMode_Start()
    {
        if (puppeteeringMode != null)
        {
            StopAllModes();
            puppeteeringMode.StartPuppeteeringMode();

            // Underline the selected mode button
            UI.SetButtonTextStyle(UI.puppeteeringModeButton, TMPro.FontStyles.Underline);

            // Activate the Puppeteering mode menu (only contains the Rotation button for now)
            UI.puppeteeringModeMenu.SetActive(true);

            Debug.Log("[PUPPETEERING] Starting the mode.");
        }
        else
        {
            Debug.Log("[PUPPETEERING] Cannot launch, the mode script is not attached to the avatar.");
        }
    }

    public void PuppeteeringMode_ToggleRotation()
    {
        puppeteeringMode.ToggleRotation();

        if (puppeteeringMode.isFreeRotation)
        {
            UI.SetButtonTextStyle(UI.rotationButton, TMPro.FontStyles.Underline);
            Debug.Log("[PUPPETEERING] Enabled the free rotation mode");
        }
        else
        {
            UI.SetButtonTextStyle(UI.rotationButton, TMPro.FontStyles.Normal);
            Debug.Log("[PUPPETEERING] Disabled the free rotation mode");
        }
    }

    public void BodytrackingMode_Start()
    {
        if (bodyTrackingMode != null)
        {
            StopAllModes();
            bodyTrackingMode.enabled = true;
        
            // Underline the selected mode button
            UI.SetButtonTextStyle(UI.bodyTrackingModeButton, TMPro.FontStyles.Underline);
            Debug.Log("[BODY TRACKING] Starting the mode. Will not work if previous logs show K4A_RESULT_FAILED.");
        }
        else
        {
            Debug.Log("[BODY TRACKING] Cannot launch, the mode script is not attached to the avatar.");
        }
    }

    public void PhysicalcontrolMode_Start()
    {
        if (physicalControlMode != null)
        {
            StopAllModes();
            physicalControlMode.enabled = true;

            // Underline the selected mode button
            UI.SetButtonTextStyle(UI.physicalcontrolModeButton, TMPro.FontStyles.Underline);
            Debug.Log("[PHYSICAL CONTROL] Starting the mode.");
        }
        else
        {
            Debug.Log("[PHYSICAL CONTROL] Cannot launch, the mode script is not attached to the avatar.");
        }
    }
}
