﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Activate GameObject at provided transform position */
public class SpawnObjects : MonoBehaviour
{
    public GameObject m_object;
    public Transform m_spawnLocation;
    
    void Start()
    {
        m_object.SetActive(false);
    }

    public void ShowObjects ()
    {
        if (m_spawnLocation)
        {  
            m_object.transform.position = m_spawnLocation.position;
        }
        m_object.SetActive(!m_object.activeSelf);
    }
}
