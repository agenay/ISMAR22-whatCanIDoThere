﻿using Microsoft.MixedReality.Toolkit.UI;
using RootMotion.Demos;
using UnityEngine;

/// <summary>
/// This mode allows manipulating the avatar with IK targets attached to its limbs (hands and feet).
/// Requires the PuppetMaster package to be imported:
///     http://root-motion.com/puppetmasterdox/html/page3.html
/// Needs to be attached to the avatar's root gameobject after having configured it with PuppetMaster.
/// </summary>
[RequireComponent(typeof(AnimatorIK))]
public class PuppeteeringMode : MonoBehaviour
{
    // IK target spheres to be instantiated in the Puppeteering mode for limb manipulation
    [SerializeField] private GameObject targetsPrefab;

    // Transforms of the avatar model's hands and feet gameobjects.
    [SerializeField] private Transform leftHand;
    [SerializeField] private Transform rightHand;
    [SerializeField] private Transform leftFoot;
    [SerializeField] private Transform rightFoot;

    private GameObject targetsParentGo;
    private RotationAxisConstraint rotationConstraint;
    private AnimatorIK animatorIK;

    // For the AvatarManager
    [HideInInspector] public bool isFreeRotation;

    private void Awake()
    {
        // Fetch components
        rotationConstraint = GetComponentInChildren<RotationAxisConstraint>();
        animatorIK = GetComponent<AnimatorIK>();

        // Intantiate the target objects to manipulate the limbs with IK and set them as child of the avatar
        targetsParentGo = Instantiate(targetsPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        targetsParentGo.transform.parent = this.gameObject.transform;
        targetsParentGo.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        // By default, lock the avatar's X and Z rotation axes.
        isFreeRotation = false;

        // Configure each target to their associated limb
        TargetAssignment targetList = targetsParentGo.GetComponent<TargetAssignment>();

        targetList.leftHandIK.GetComponent<FixedPerimeterConstraint>().ConstraintTransform = leftHand;
        animatorIK.leftHandIKTarget = targetList.leftHandIK.transform;

        targetList.rightHandIK.GetComponent<FixedPerimeterConstraint>().ConstraintTransform = rightHand;
        animatorIK.rightHandIKTarget = targetList.rightHandIK.transform;

        targetList.leftFootIK.GetComponent<FixedPerimeterConstraint>().ConstraintTransform = leftFoot;
        animatorIK.leftFootIKTarget = targetList.leftFootIK.transform;

        targetList.rightFootIK.GetComponent<FixedPerimeterConstraint>().ConstraintTransform = rightFoot;
        animatorIK.rightFootIKTarget = targetList.rightFootIK.transform;

        // Place the targets at the transform location of their associated limbs
        targetList.leftHandIK.transform.position = leftHand.position;
        targetList.rightHandIK.transform.position = rightHand.position;
        targetList.leftFootIK.transform.position = leftFoot.position;
        targetList.rightFootIK.transform.position = rightFoot.position;
    }

    // When pressing the "toggle rotation" button, enables free rotation (or disables it)
    public void ToggleRotation()
    {
        if (rotationConstraint.enabled)
        {
            rotationConstraint.enabled = false;
            isFreeRotation = true;
        }
        else
        {
            rotationConstraint.enabled = true;
            isFreeRotation = false;

            // Reset the rotation to make the avatar vertical in case it was turned
            transform.rotation = new Quaternion(0f, transform.rotation.y, 0f, 0f);
        }
    }

    public void StartPuppeteeringMode()
    {
        // By default, enable rotation constraints
        rotationConstraint.enabled = true;

        // Enable the manipulation targets
        targetsParentGo.SetActive(true);

        // Start IK
        animatorIK.ikActive = true;
    }

    public void StopPuppeteeringMode()
    {
        // Hide the IK target spheres (gameobjects)
        targetsParentGo.SetActive(false);

        // Disable the IK
        animatorIK.ikActive = false;
        
        // Disable the free rotation mode
        rotationConstraint.enabled = true;
    }
}
