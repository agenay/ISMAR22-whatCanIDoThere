﻿using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

/// <summary>
/// This mode allows moving the avatar with a physical input device (keyboard, gamepad). Was tested
/// with an XBOX X/S series controller only. This code was inspired by Ketra Games (https://www.ketra-games.com/) 
/// tutorial on how to create a 3D platformer in Unity. You can find the link to the videos here:
///     https://youtube.com/playlist?list=PLx7AKmQhxJFaj0IcdjGJzIq5KwrIfB1m9
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class PhysicalControlMode : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 800f;
    [SerializeField]
    private float jumpSpeed = 5f;
    [SerializeField]
    private float jumpButtonGracePeriod = 0.2f;
    [SerializeField]
    private float jumpHorizontalSpeed = 3f;

    private Animator animator;
    private CharacterController characterController;
    private float ySpeed;
    private float originalStepOffset;
    private float? lastGroundedTime;
    private float? jumpButtonPressedTime;
    private bool isJumping;
    private bool isGrounded;

    // Fetch the required components
    void Awake()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        originalStepOffset = characterController.stepOffset;

        // Initialize the avatar's collider size to the mesh size
        characterController.height = GetComponentInChildren<SkinnedMeshRenderer>().bounds.size.y;
        characterController.radius = GetComponentInChildren<SkinnedMeshRenderer>().bounds.size.z;

        // If you don't use the body tracking mode (which overrides the collider position), apply this
        if (GetComponent<BodyTrackingMode>() == null)
        {
            characterController.center = new Vector3(0, characterController.height / 2, 0);  
        }
    }

    // The avatar is moved through this Update function
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        float inputMagnitude = Mathf.Clamp01(movementDirection.magnitude);

        // Crouch
        if (Input.GetAxis("Crouch") == 1)
        {
            animator.SetBool("IsCrouching", true);
        }
        else
        {
            animator.SetBool("IsCrouching", false);
        }

        // T-pose
        if (Input.GetButton("T-pose"))
        {
            animator.SetBool("IsTPosing", true);
        }
        else
        {
            animator.SetBool("IsTPosing", false);
        }

        // Sit
        if (Input.GetButton("Sit"))
        {
            animator.SetBool("IsSitting", true);
        }

        // Lay down
        if (Input.GetAxis("Lie Down") == -1)
        {
            animator.SetBool("IsLaying", true);
        }
        else if (Input.GetAxis("Lie Down") == 1)
        {
            animator.SetBool("IsLaying", false);
        }

        // Controls the blend of the idle, walk, and run animations
        animator.SetFloat("Input Magnitude", inputMagnitude, 0.05f, Time.deltaTime);

        // Direction of the avatar
        movementDirection.Normalize();

        // Calculate the speed on the y axis based on if the jump button was pressed
        ySpeed += Physics.gravity.y * Time.deltaTime;

        if (characterController.isGrounded)
        {
            lastGroundedTime = Time.time;
        }

        if (Input.GetButtonDown("Jump"))
        {
            jumpButtonPressedTime = Time.time;
        }

        if (Time.time - lastGroundedTime <= jumpButtonGracePeriod)
        {
            characterController.stepOffset = originalStepOffset;
            ySpeed = -0.5f;
            animator.SetBool("IsGrounded", true);
            isGrounded = true;
            animator.SetBool("IsJumping", false);
            isJumping = false;
            animator.SetBool("IsFalling", false);

            if (Time.time - jumpButtonPressedTime <= jumpButtonGracePeriod)
            {
                ySpeed = jumpSpeed;
                animator.SetBool("IsJumping", true);
                isJumping = true;
                jumpButtonPressedTime = null;
                lastGroundedTime = null;
            }
        }
        else
        {
            characterController.stepOffset = 0;
            animator.SetBool("IsGrounded", false);
            isGrounded = false;

            // Falling: check if ySpeed is negative or when dropping down from a platform
            // (but not when going down stairs, etc.)
            if ((isJumping && ySpeed < 0) || ySpeed < -2)
            {
                animator.SetBool("IsFalling", true);
            }
        }

        if (movementDirection != Vector3.zero)
        {
            animator.SetBool("IsMoving", true);
            animator.SetBool("IsLaying", false);
            animator.SetBool("IsSitting", false);

            Quaternion toRotation = Quaternion.LookRotation(movementDirection, Vector3.up);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
        else
        {
            animator.SetBool("IsMoving", false);
        }

        // Jumping
        if (isGrounded == false)
        {
            Vector3 velocity = movementDirection * inputMagnitude * jumpHorizontalSpeed;
            velocity.y = ySpeed;

            characterController.Move(velocity * Time.deltaTime);
        }
    }

    // Use root motion of the animation clips to move the avatar (except when jumping)
    private void OnAnimatorMove()
    {
        if (isGrounded)
        {
            // Calculate the position change determined by the animation.
            Vector3 velocity = animator.deltaPosition;
            velocity.y = ySpeed * Time.deltaTime;

            characterController.Move(velocity);
        }
    }
}
