﻿using UnityEngine;

public class TargetAssignment : MonoBehaviour
{
    public GameObject leftHandIK;
    public GameObject rightHandIK;
    public GameObject leftFootIK;
    public GameObject rightFootIK;
}
